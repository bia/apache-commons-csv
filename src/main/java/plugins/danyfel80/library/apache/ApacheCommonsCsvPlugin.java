package plugins.danyfel80.library.apache;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;

/**
 * Icy wrapper for the Apache Commons CSV library.
 * @author Daniel Felipe Gonzalez Obando
 */
public class ApacheCommonsCsvPlugin extends Plugin implements PluginLibrary
{

}
